import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgregarComponent } from './components/routes/agregar/agregar.component';
import { DashboardComponent } from './components/routes/dashboard/dashboard.component';
import { DetailsComponent } from './components/routes/details/details.component';
import { HomeComponent } from './components/routes/home/home.component';
import { LoginComponent } from './components/routes/login/login.component';
import { MyListComponent } from './components/routes/my-list/my-list.component';
import { PeliculasComponent } from './components/routes/peliculas/peliculas.component';
import { SeriesComponent } from './components/routes/series/series.component';
import { SignupComponent } from './components/routes/signup/signup.component';
import { AuthGuardGuard } from './guards/auth-guard.guard';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'peliculas',
    component: PeliculasComponent
  },
  {
    path: 'series',
    component: SeriesComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'mylist',
    component: MyListComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'agregar',
    component: AgregarComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'series/details',
    component: DetailsComponent
  },
  {
    path: 'movies/details',
    component: DetailsComponent
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
