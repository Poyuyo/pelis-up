import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public userLogged : Observable<any> = this._authService.afAuth.user;

  constructor(private _authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  logout(){
    this._authService.logout()
    localStorage.removeItem('usuario')
    this.router.navigate(['home'])
  }

}
