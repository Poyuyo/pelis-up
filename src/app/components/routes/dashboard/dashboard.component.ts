import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { MoviesService } from 'src/app/services/movies.service';
import { count } from 'console';
import { MoviesSeriesApi } from 'src/interfaces/MoviesSeriesApi';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private _authService: AuthService, private router: Router, private _moviesService: MoviesService) { }
  user: any;
  countMovie: number = 0;
  countTv: number = 0;
  movies_series: MoviesSeriesApi[] = [];

  async ngOnInit(): Promise<void> {
    this.user = JSON.parse(localStorage.getItem('usuario') || '{}')
    this.countTtems('movie')
    this.countTtems('tv')
  }

  goToList(type: string) {
    let title: string;
    if (type == 'movie')
      title = 'Películas'
    else
      title = 'Series'
    this.router.navigate(['mylist'], { queryParams: { type: type, title: title } });
  }

  countTtems(type: string) {

    //El contador mas ineficiente de la historia MEJORAR
    this._moviesService.getMyList(this.user.uid, type).subscribe(
      response => {
        this.movies_series = [];
        response.forEach((element: any) => {
          this.movies_series.push({
            globalId: element.payload.doc.id,
            ...element.payload.doc.data()
          });

        });
        if (type == 'movie')
          this.countMovie = this.movies_series.length;
        else
          this.countTv = this.movies_series.length;
      },
      error => {
        console.log("Fallo la peticion: ", error);
      }

    )
  }

}
