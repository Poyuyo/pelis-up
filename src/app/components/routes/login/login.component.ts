import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  miFormulario: FormGroup = this.fb.group({
    email: [, [Validators.required, Validators.email]],
    password: [, [Validators.required, Validators.minLength(6)]],
  });


  user: any;

  constructor(private _authService: AuthService, private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.validateUser()
  }

  validateUser() {
    if (localStorage.getItem('usuario')) {
      this.router.navigate(['dashboard']);
    }
    
  }

  async loginWithGoogle() {
    this._authService.loginWithGoogle()
      .then(response => {
        if (!response)
          return
        localStorage.setItem('usuario', JSON.stringify(response.user))
        this.router.navigate(['dashboard'])
      })
  }

  async login() {
    const {email, password} = this.miFormulario.controls;
    this._authService.login(email.value, password.value)
      .then(response => {
        if (!response)
          return
        localStorage.setItem('usuario', JSON.stringify(response.user))
        this.router.navigate(['dashboard'])
      })
  }

  campoEsValido(campo: string) {
    return (
      this.miFormulario.controls[campo].errors &&
      this.miFormulario.controls[campo].touched
    );
  }

}
