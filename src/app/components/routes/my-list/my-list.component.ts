import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { MoviesService } from 'src/app/services/movies.service';
import { MoviesSeriesApi } from 'src/interfaces/MoviesSeriesApi';

@Component({
  selector: 'app-my-list',
  templateUrl: './my-list.component.html',
  styleUrls: ['./my-list.component.css']
})
export class MyListComponent implements OnInit {

  user: any;

  title: string = '';

  toSearch: string = "";
  media_type: string = "";
  text: string = '';

  movies_series: MoviesSeriesApi[] = [];
  movies_series_show: MoviesSeriesApi[] = [];
  movies_series_search: MoviesSeriesApi[] = [];

  constructor(private _moviesService: MoviesService, private _authService: AuthService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.media_type = this.route.snapshot.queryParams['type'];


    if (this.media_type == 'movie')
      this.text = "Buscar películas"
    else
      this.text = "Buscar series"

    this.title = this.route.snapshot.queryParams['title'];
    this.user = JSON.parse(localStorage.getItem('usuario') || '{}')
    this.getTrending()
  }


  async getTrending() {

    this._moviesService.getMyList(this.user.uid, this.media_type).subscribe(
      response => {
        this.movies_series_show = [];
        response.forEach((element: any) => {
          this.movies_series_show.push({
            globalId: element.payload.doc.id,
            ...element.payload.doc.data()
          });

          this.movies_series.push({
            globalId: element.payload.doc.id,
            ...element.payload.doc.data()
          });
        });
      },
      error => {
        console.log("Fallo la peticion: ", error);
      }

    )

  }

  setLists(data: any) {
    this.movies_series = data.results;
    this.movies_series_show = data.results;
  }

  searchMovieOrSerie(e: string) {

    this.movies_series_search = [];
    this.toSearch = e.toUpperCase();

    this.movies_series_search = this.movies_series.filter(movie_serie => (movie_serie.title + "").concat(movie_serie.name + "").toUpperCase().includes(this.toSearch))

    if (e !== '') {
      this.movies_series_show = this.movies_series_search
    } else {
      this.movies_series_show = this.movies_series;
    }
  }

}
