import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  miFormulario: FormGroup = this.fb.group({
    email: [, [Validators.required, Validators.email]],
    password: [, [Validators.required, Validators.minLength(6)]],
  });

  constructor(private _authService: AuthService, private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
  }

  signup() {
    this._authService.signup(this.miFormulario.controls['email'].value, this.miFormulario.controls['password'].value)
      .then((response) => {
        if (response)
        localStorage.setItem('usuario', JSON.stringify(response.user))
          this.router.navigate(['dashboard'])

      })
  }

  campoEsValido(campo: string) {
    return (
      this.miFormulario.controls[campo].errors &&
      this.miFormulario.controls[campo].touched
    );
  }

}
