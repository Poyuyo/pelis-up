import { Injectable } from '@angular/core';
import firebase from 'firebase/compat/app';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public afAuth: AngularFireAuth, private router: Router) { }

  async login(email: string, password: string) {

    return await this.afAuth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        console.log("Login email y pass exitoso")
        return result;
      })
      .catch((err) => {
        console.log("Error en login", err)
        return null;
      })
  }

  async loginWithGoogle() {
    return await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then((result) => {
        return result;
      })
      .catch((err) => {
        console.log("Error en login con google ", err)
        return null;
      })
  }

  async signup(email: string, password: string) {
    return this.afAuth.createUserWithEmailAndPassword(email, password)
      .then((userCredential) => {
        return userCredential;
      })
      .catch((error) => {
        console.log("Error en register: ", error)
        return null;
      });
  }

  logout() {
    this.afAuth.signOut();
  }

}
