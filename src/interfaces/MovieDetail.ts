export interface MovieDetail {
    backdrop_path:         string;
    genres:                Genre[];
    id:                    number;
    original_title:        string;
    overview:              string;
    poster_path:           string;
    release_date:          Date;
    runtime:               number;
    tagline:               string;
    title:                 string;
    vote_average:          number;
}

export interface Genre {
    id:   number;
    name: string;
}