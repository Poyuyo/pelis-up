export interface MoviesSeriesApi {
    id:                number;
    poster_path:       string;
    title?:            string;
    vote_average:      number;
    name?:             string;
    media_type:        string;
    globalId?:          string
}

export interface MovieSerieUser extends  MoviesSeriesApi{
    idUser: string;
}