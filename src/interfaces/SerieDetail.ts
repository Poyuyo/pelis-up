export interface SerieDetail {
    backdrop_path:        string;
    episode_run_time:     number[];
    first_air_date:       Date;
    genres:               Genre[];
    id:                   number;
    last_air_date:        Date;
    name:                 string;
    number_of_episodes:   number;
    number_of_seasons:    number;
    overview:             string;
    poster_path:          string;
    status:               string;
    tagline:              string;
    vote_average:         number;
}

export interface Genre {
    id:   number;
    name: string;
}
